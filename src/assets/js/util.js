import {capitalize} from 'lodash'
import $ from 'jquery'
import 'slick-carousel'
const getPrimer = () => {
    return capitalize("primer")
}

const initHomeSlider = () => {
    $('#slider').slick({
        infinite: true,
        speed: 800,
        dots: true,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
    })
}

export {
    getPrimer,
    initHomeSlider
}