import {initHomeSlider} from "./util";
import {initHeader} from "./components/header"

const main = () => {
    initHomeSlider()
    initHeader()
}

main()