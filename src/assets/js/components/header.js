import onChange from 'on-change';

const initHeader = () => {
    // States
    const state = {
        activeMenu: {
            active: false
        }
    }

    //onChange
    const menuNavs = document.querySelector('.menu__nav')
    const watchedMenu = onChange(state, (path, value) => {
            if (value) {
                if (!menuNavs.classList.contains('menu__nav--active')) {
                    menuNavs.classList.add('menu__nav--active')
                }
            } else {
                if (menuNavs.classList.contains('menu__nav--active')) {
                    menuNavs.classList.remove('menu__nav--active')
                }
            }
        }
    )

    //changeState
    const menuHandler = (evt) => {
        evt.preventDefault()
        evt.stopImmediatePropagation()
        watchedMenu.activeMenu.active = watchedMenu.activeMenu.active === false;
    }

    //event
    const menuButton = document.querySelector('.burger__menu')
    menuButton.addEventListener('click', menuHandler)
}

export {
    initHeader
}